/**
 * Loads partial into document
 */
async function loadContent() {
	// aktuellen Pfad aus dem browser auslesen
	const path = window.location.pathname;
	// Aus routes die gemappte Datei auslesen, oder einen 404 error werfen.
	// variable = expression -> right to left -> zuerst wird expression ausgewertet, dann das resultat zugewiesen
	const route = routes[path] || routes[404];
	// laden des Inhalts an der route
	const reply = await fetch(route)
		// den Text der response reply zuweisen
		.then((data) => {
			console.log(data);
			return data.text()
		});

	// Inhalt austauschen in mainContent
	mainContent.innerHTML = reply;
}

/*
	Routes:
	die keys der Objekte sind urls, die wir bei Klick auslesen.
	den Keys sind Dokumente zugeordnet, die wir laden
*/
const routes = {
	404: "/pages/404.html",
	"/": "/pages/home.html",
	"/page1": "/pages/page1.html",
	"/page2": "/pages/page2.html",
	"/page3": "/pages/page3.html"
}

// DOM Elemente selektieren
const mainNav = document.getElementById('mainNav');
const mainContent = document.getElementById('mainContent');

// Event Listener auf parent, um auf alle Klicks in den Links im parent zu reagieren
mainNav.addEventListener('click', function (e) {
	// Prüfen, ob das geklickte Element ein Link ist
	if (e.target.tagName === 'A') {
		// Verhindern, dass der Link ausgeführt wird
		e.preventDefault();
		// geklickte Information (href) in history speichern
		// daher können wir es nun in loadContent auslesen und
		// die route identifizieren.
		window.history.pushState({}, "", e.target.href);
		loadContent();
	}
});

// init
// forward / back buttons
window.onpopstate = loadContent;
// default content laden
loadContent();
